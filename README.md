# rpg-characters

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Object oriented assignment that create character classes for an RPG game.

## Table of Contents

-   [Install](#install)
-   [Usage](#usage)
-   [Maintainers](#maintainers)
-   [Contributing](#contributing)
-   [License](#license)

## Install

```
Open project in Visual Studio 2022 with .Net 6 and build the application.
```

## Usage

```
Run tests in tests project. Run with debugger for character sheet.
```

## Class diagram

![Class diagram](RPGCharacters/ClassDiagram.png)

## Maintainers

@mikaelb

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Mikael Bjerga
