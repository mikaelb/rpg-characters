﻿using RPGCharacters.CharacterClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.ItemsClasses
{
	/// <summary>
	/// Creates the abstract item class. Used for both Weapon and Armor. Stores item name, level and equipment slot used.
	/// </summary>
	public abstract class Item
	{
		public string ItemName { get; set; }
		public int ItemLevel { get; set; }
		public abstract Slot ItemSlot { get; set; }
	}
}
