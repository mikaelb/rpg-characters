﻿using RPGCharacters.CharacterClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.ItemsClasses
{
	/// <summary>
	/// Defines the item object weapon. Storing weapon type, item slot it uses and weapon attributes.
	/// </summary>
	public class Weapon : Item
	{
		public WeaponType WeaponType { get; set; }
		public override Slot ItemSlot { get; set; }
		public WeaponAttributes WeaponAttributes { get; set; }
	}
}
