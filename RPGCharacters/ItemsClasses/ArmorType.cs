﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.ItemsClasses
{
	/// <summary>
	/// Creates an enum with all the possible types of armor a character might be able to equip.
	/// </summary>
	public enum ArmorType
	{
		ARMOR_CLOTH,
		ARMOR_LEATHER,
		ARMOR_MAIL,
		ARMOR_PLATE
	}
}
