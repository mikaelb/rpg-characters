﻿using RPGCharacters.CharacterClasses;
using RPGCharacters.ItemsClasses;

namespace RPGCharacters
{
	class Program
	{
		/// <summary>
		/// A simple test for showing character sheet.
		/// </summary>
		/// <param name="args"></param>
		static void Main(string[] args)
		{
			Ranger character = new("Mikael");

			Weapon Bow = new()
			{
				ItemName = "Common bow",
				ItemLevel = 1,
				ItemSlot = Slot.SLOT_WEAPON,
				WeaponType = WeaponType.WEAPON_BOW,
				WeaponAttributes = new WeaponAttributes() { Damage = 12, AttackSpeed = 0.8 }
			};

			Armor LeatherBody = new()
			{
				ItemName = "Common leather armor",
				ItemLevel = 1,
				ItemSlot = Slot.SLOT_BODY,
				ArmorType = ArmorType.ARMOR_LEATHER,
				Attributes = new PrimaryAttributes() { Dexterity = 1 }
			};
			character.LevelUp();
			character.Equip(Bow);
			character.Equip(LeatherBody);
			Console.WriteLine(CharacterSheet.PrintCharacterSheet(character));
		}
	}
}