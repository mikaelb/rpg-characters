﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Exceptions
{
	public class InvalidWeaponException : Exception
	{
		/// <summary>
		/// Custom exception for errors with weapon items.
		/// </summary>
		/// <param name="message"></param>
		public InvalidWeaponException(string? message) : base(message)
		{
		}
	}
}
