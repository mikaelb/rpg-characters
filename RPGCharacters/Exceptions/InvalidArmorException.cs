﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Exceptions
{
	public class InvalidArmorException : Exception
	{
		/// <summary>
		/// Custom exception for errors with armor items.
		/// </summary>
		/// <param name="message"></param>
		public InvalidArmorException(string? message) : base(message)
		{
		}
	}
}
