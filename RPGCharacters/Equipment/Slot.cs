﻿namespace RPGCharacters.ItemsClasses
{
	/// <summary>
	/// Creates an enum with all the possible equipment slots a character might have.
	/// </summary>
	public enum Slot
	{
		SLOT_HEAD,
		SLOT_BODY,
		SLOT_LEGS,
		SLOT_WEAPON
	}
}