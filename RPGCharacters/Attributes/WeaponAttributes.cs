﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.ItemsClasses
{
	/// <summary>
	/// Creates the weapon attributes that stores the weapons damage, attack speed and damage per second
	/// </summary>
	public class WeaponAttributes
	{
		public double Damage { get; set; }
		public double AttackSpeed { get; set; }
		public double DPS => Damage * AttackSpeed;
	}
}